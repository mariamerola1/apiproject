# **Virtualised Integration Testcase Creation Description** 
Powered by IBM Wazi Virtual Test Platform

## Record The Following Tests to Run a VTP Integration Test This Application

### INQUIRE CUSTOMER
> customer record 1 with full data returned

> customer record 20 with only required fields 

> customer record 4000 or above for no record found

### UPDATE CUSTOMER
> update existing customer 1-4000 (all fields)

> update existing customer 1-4000 (one field)

### DELETE CUSTOMER
> delete existing customer 1-4000 (all fields)

> delete existing customer 1-4000 (one field)
